<?php namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

    public function transform(User $user)
    {
        return [
            'id'           => $user->id,
            'name'         => $user->first_name . ' ' . $user->last_name,
            'username' => $user->username,
            'email'   => $user->email,
            'role'   => $user->roles->role,
            //'role'   => $user->roles,
            'created_at'   => $user->created_at->format('Y-m-d')
        ];
    }
}
