<?php namespace App\Exceptions;

use Exception;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Log;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        'Symfony\Component\HttpKernel\Exception\HttpException'
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        //echo ('report');
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        //dd($request->user());
        //Log::error($request->fullUrl());

        // $request->user() // get the user

        $response = [
            'version' => config('options.version'),
            'success' => 'false',
            //'message' => $e->getMessage(),
            //'code' => $e->getStatusCode(),
        ];
        Log::error($request->fullUrl(), $response);
        //return response()->json($response);
        return parent::render($request, $e);
    }

}
