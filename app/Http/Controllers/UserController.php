<?php namespace App\Http\Controllers;

use Validator;
use App;
use App\Models\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;


class UserController extends APIController
{
    protected $user;

    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function error() {
        abort(403, 'Unauthorized action');
    }

    public function index(Manager $fractal, UserTransformer $UserTransformer) {
        $users = $this->user->with('roles')->where('active', 1)->get();
        $collection = new Collection($users, $UserTransformer);
        $data = $fractal->createData($collection)->toArray();
        return $this->respond($data);
    }


    public function show($id, Manager $fractal, UserTransformer $UserTransformer){
        $user = $this->user->find($id); //findOrFail
        if($user == null) {
            return $this->respondWithError('User Not found');
        }
        $item = new Item($user, $UserTransformer);
        $data = $fractal->createData($item)->toArray();
        return $this->respond($data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:150',
            'last_name' => 'required|max:150',
            'username' => 'required|unique:users|max:150',
            'email' => 'required|email:unique:users|max:150',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return $this->respondWithError(['errors' => $validator->errors()]);
        }

        $this->user->first_name = $request->input('first_name');
        $this->user->last_name = $request->input('last_name');
        $this->user->username = $request->input('username');
        $this->user->email = $request->input('email');
        $this->user->password = Hash::make($request->input('password'));
        $this->user->save();
        return $this->respondCreated('User created');
    }

    public function edit(Request $request, $id)
    {
        //$request->all()
        $first_name = $request->input('first_name');

//        $validate = $this->validate($request, [
//            'first_name' => 'required|max:50',
//            'lastname' => 'required|max:50',
//            'username' => 'required|max:50',
//            'email' => 'required|unique:posts|max:50'
//        ]);

        //dd($validate);
        $validate = true;
        return response()->json(['data' => $validate, 'env' => App::environment()]);

//        $validator = Validator::make($request->all(), [
//            'title' => 'required|unique:posts|max:255',
//            'body' => 'required',
//        ]);
//        if ($validator->fails()) {
//            return redirect('errors')
//                ->withErrors($validator)
//                ->withInput();
//        }
    }
}
