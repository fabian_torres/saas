<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use League\Fractal\Manager;

class APIController extends Controller {
    /**
     * @var int
     */
    protected $statusCode = IlluminateResponse::HTTP_OK;
    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    /**
     * @param mixed $statusCode
     * @return mixed
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }
    /**
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = 'Nije pronađeno!')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }
    /**
     * @param string $message
     * @return mixed
     */
    public function respondInternalError($message = 'Dogodila se greška!')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }
    /**
     * @param $message
     * @return mixed
     */
    public function respondCreated($message)
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_CREATED)->respond([
            'message' => $message
        ]);
    }
    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondOk($message)
    {
        return $this->respond([
            'success' => true,
            'message' => $message
        ]);
    }
    /**
     * @param string $message
     * @return mixed
     */
    public function respondWithValidationError($message = 'Neuspješna validacija!')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)->respondWithError([
            'message' => $message
        ]);
    }
    /**
     * @param $data
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($data, $headers = [], $message = '')
    {
        $response = $data;

        if(!array_key_exists('success', $data)) {
            $response['success'] = true;
        }

        if(!array_key_exists('message', $data) && $message) {
            $response['message'] = $message;
        }

        return response()->json($response, $this->getStatusCode(), $headers);
    }
    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'success' => false,
            'message'     => $message,
            'code' => $this->getStatusCode()
        ]);
    }
    public function respondWithCORS($data)
    {
        return $this->respond($data, $this->setCORSHeaders());
    }
    private function setCORSHeaders()
    {
        $header['Access-Control-Allow-Origin'] = '*';
        $header['Allow'] = 'GET, POST, OPTIONS';
        $header['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, X-Request-With';
        $header['Access-Control-Allow-Credentials'] = 'true';
        return $header;
    }

//    public function json(Manager $fractal, $data)
//    {
//        $data = $fractal->createData($data)->toArray();
//        $this->respond($data);
//    }
}
