<?php

class UserTest extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function testEndPoint()
    {
        $this->assertTrue(true);
    }

    public function testUsers()
    {
        $this->get('/users/2')->seeJson([
            'first_name' => 'Beverly',
            'id' => 2,
        ]);
    }
}
